import React from 'react'

import {Form} from "./components/Form";
import {TasksBox} from "./components/TasksBox";

import {StoreProvider} from "./stores/useStore";
import {GlobalStore} from "./stores/GlobalStore";

const stores = {
    globalStore: new GlobalStore(),
};


const App = () => {

  return (
      <StoreProvider store={stores}>
        <div className="App">

          <Form />

          <TasksBox/>
        </div>
      </StoreProvider>
  );
}

export default App;
