import React, {useState} from 'react'

import {useStore} from "../stores/useStore";

export const Form = () => {
    const [newTaskValue, setNewTaskValue] = useState('')
    const {globalStore} = useStore()

    const onSubmit = e => {
        e.preventDefault()
        globalStore.pushToTasks({
            title: newTaskValue,
            isComplete: false,
            id: globalStore.tasks.length + 1
        })
    }

    return (
        <form onSubmit={onSubmit}>
            <input value={newTaskValue} onChange={e => setNewTaskValue(e.target.value)} placeholder='New task'/>
            <button type='submit'>Add</button>
        </form>
    )
}