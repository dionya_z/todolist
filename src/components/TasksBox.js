import React from 'react'
import {observer} from "mobx-react";

import {useStore} from "../stores/useStore";

const Component = () => {
    const {globalStore} = useStore()

    return (
        <div>
            <h1>My tasks</h1>

            {globalStore.tasks.map((task, index) => (
                <div key={task.id}><input onChange={() => globalStore.invertIsComplete(index)} type='checkbox' id={task.id} checked={task.isComplete} /><label htmlFor={task.id}>{task.title}</label></div>
            ))}
        </div>
    )
}

export const TasksBox = observer(Component)